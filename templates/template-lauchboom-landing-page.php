<?php 
/*
Template Name: Launchboom Landing Page
*/
?>
<?php get_header(); ?>
<?php $img = get_field('background_image'); ?>
<style>
	.video-hero:before{
		background-image: url('<?php echo $img['url']; ?>');
	}
</style>

<?php 
$video_type = get_field('youtube_or_vimeo');

	if($video_type == 'Vimeo'): 
		$class = "video-player--vimeo"; 
	else: 
		$class = "video-player--youtube"; 
	endif;

$class_general = ' video-cta button ghost-button white not-animated';
$class = $class . $class_general;
?>

<div class="video-full">
	<div class="video-wrap">
		<div class="video-hero home-hero">
			<div class="video-holder from-b-up video-wrap"  id="video-wrap">
				<div class="video-cover"></div>
				<?php $video = get_field('background_video'); ?>
				<?php if (!empty($video)): ?>
					<video id="video-hero" class="" height="100%" muted="muted" width="auto" preload="auto" autoplay loop> 
			            <source src="<?php tf('background_video'); ?>" type="video/mp4">
			        </video>
			    <?php endif; ?>
			</div>
			<div class="video-content home-video-content">
				<div class="cf video-content-top-bar">
					<?php svg_embed('logo', 'video-logo not-animated default-animate'); ?>
					<a href="<?php tf('top_button_link'); ?>" class=" not-animated default-animate button button-bigger ghost-button white <?php if (get_field('top_button_link_on_page') == 'Yes'): echo "roller"; endif; ?>"><?php tf('top_button_text'); ?></a>
				</div>
				<h1 class="bigger-title not-animated default-animate white"><?php tf('headline'); ?></h1>
				<!-- <h2 class="not-animated default-animate"><?php //tf('subheadline'); ?></h2> -->
				<?php 
					$link = get_field('video_id'); 
					if (!empty($link)):
						$button_class = $class; 
					else:
						$button_class = $class_general . ' roller';
					endif;
				?>
				<?php button('#main-content', get_field('button_text'), $button_class); ?>
			</div>
		</div>
	</div>
	<div class="embedded-video-wrap video-wrap--height">
		<?php if(get_field('youtube_or_vimeo') == 'Vimeo'): ?>
			<span class="video-pauser video-pauser--vimeo x-button posa">x</span>
		<?php endif; ?>
		<div class="vidrap" id="hidden_video">
			<?php if(get_field('youtube_or_vimeo') == 'Vimeo'): ?>
				<iframe src="https://player.vimeo.com/video/<?php tf('video_id'); ?>?title=0&byline=0&portrait=0&api=1&player_id=hidden_vimeo" width="1400" height="787" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen id="hidden_vimeo"></iframe>
			<?php else: ?>
				<iframe width="1400" height="788" class="vid" id="hidden_youtube" src="https://www.youtube.com/embed/<?php tf('video_id'); ?>?modestbranding=1&rel=0" frameborder="0" allowfullscreen></iframe>
			<?php endif; ?>
		</div>
	</div>
</div>
<div id="main-content">
	
</div>

<?php get_footer(); ?>