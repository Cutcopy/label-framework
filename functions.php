<?php

/*
Author: Label Creative
URL: htp://thelabelcreative.com/

*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/label.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once( 'library/label.php' ); // if you remove this, label will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
// require_once( 'library/news-post-type.php' ); 
/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
require_once( 'library/admin.php' ); // this comes turned off by default
/*
4. library/acf.php
	- adding custom acf functions to make things a little easier
*/
require_once( 'library/acf.php' ); 


/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'label-thumb-600', 600, 150, true );
add_image_size( 'label-thumb-300', 300, 100, true );
/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'label-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'label-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function label_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'labeltheme' ),
		'description' => __( 'The first (primary) sidebar.', 'labeltheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'labeltheme' ),
		'description' => __( 'The page sidebar.', 'labeltheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	register_sidebar( array(
	'name' => 'Footer Sidebar 1',
	'id' => 'footer-sidebar-1',
	'description' => 'Appears in the footer area',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widgettitle">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => 'Footer Sidebar 2',
	'id' => 'footer-sidebar-2',
	'description' => 'Appears in the footer area',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widgettitle">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => 'Footer Sidebar 3',
	'id' => 'footer-sidebar-3',
	'description' => 'Appears in the footer area',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widgettitle">',
	'after_title' => '</h3>',
	) );
	register_sidebar( array(
	'name' => 'Footer Sidebar 4',
	'id' => 'footer-sidebar-4',
	'description' => 'Appears in the footer area',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h3 class="widgettitle">',
	'after_title' => '</h3>',
	) );


	// To call the sidebar in your template, you can just copy
	// the sidebar.php file and rename it to your sidebar's name.
	// So using the above example, it would be:
	// sidebar-sidebar2.php


} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/


////a custom excerpt length 

// TRUNCK STRING TO SHORT THE STRINGS
function trunck_string($str = "", $len = 150, $more = 'true') {

    if ($str == "") return $str;
    if (is_array($str)) return $str;
    $str = strip_tags($str);    
    $str = trim($str);
    // if it's les than the size given, then return it

    if (strlen($str) <= $len) return $str;

    // else get that size of text
    $str = substr($str, 0, $len);
    // backtrack to the end of a word
    if ($str != "") {
      // check to see if there are any spaces left
      if (!substr_count($str , " ")) {
        if ($more == 'true') $str .= "...";
        return $str;
      }
      // backtrack
      while(strlen($str) && ($str[strlen($str)-1] != " ")) {
        $str = substr($str, 0, -1);
      }
      $str = substr($str, 0, -1);
      if ($more == 'true') $str;
      if ($more != 'true' and $more != 'false') $str .= $more;
    }
    return $str;
}
// the_content() WITHOUT IMAGES
// GET the_content() BUT EXCLUDE <img> OR <img/> TAGS THEN ECHO the_content()
function custom_excerpt( $Trunckvalue = null ) {
    ob_start();
    the_content();
    $postOutput = preg_replace('/<img[^>]+./','', ob_get_contents());
    ob_end_clean();
    echo trunck_string( $postOutput, $Trunckvalue, true ); ?>
    <?php
}



//// if is a parent of a specific page

// usage
/*
if (is_tree(2)) {
   // stuff
}
*/
function is_tree($pid) {
  global $post;

  $ancestors = get_post_ancestors($post->$pid);
  $root = count($ancestors) - 1;
  $parent = $ancestors[$root];

  if(is_page() && (is_page($pid) || $post->post_parent == $pid || in_array($pid, $ancestors))) {
    return true;
  } else {
    return false;
  }
};



// turn off page comments
// function default_comments_off( $data ) {
//     if( $data['post_type'] == 'page' && $data['post_status'] == 'auto-draft' ) {
//         $data['comment_status'] = 0;
//     } 

//     return $data;
// }
// add_filter( 'wp_insert_post_data', 'default_comments_off' );

// change search form
function html5_search_form( $form ) {
    $form = '<form role="search" method="get" id="searchform" class="cf searchform" action="' . home_url( '/blog/' ) . '" >
    <input type="search" placeholder="'.__("Search our blog").'" value="' . get_search_query() . '" name="s" id="s" />
    <input type="submit" id="searchsubmit" value="Search" />
    </form>';

    return $form;
}
add_filter( 'get_search_form', 'html5_search_form' );


// fancy button
function button($link, $text = "Watch Video", $class = ""){
	echo '<a href="'.$link.'" class="cf button play-button video-player '.$class.'">';
		echo '<span>'.$text.'</span>';
		echo '<svg class="play-button-arrow" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			width="44px" height="40px" viewBox="0 0 44 40" enable-background="new 0 0 44 40" xml:space="preserve">
			<polygon fill="none" stroke="#FFFFFF" stroke-miterlimit="10" points="14.637,12.633 29.52,21.225 14.637,29.818 "/>
			</svg>';
		
	echo '</a>';
}