<footer class="footer" id="footer" role="contentinfo">

    <div class="cf inner-footer mw-1040 p1">
        <div id="footer-sidebar1" class="threecol first">
            <a href="<?php bloginfo('url'); ?>"><?php svg_options_embed('logo'); ?></a>
        </div>
        <div id="footer-sidebar3" class="from-s-up-fourcol threecol">
            <h3>Contact</h3>
            <?php
                $phone = get_field('phone', 'option');
                $phone = preg_replace('/\D+/', '', $phone);
            ?>
            <ul class="ma">
                <li>
                    <a href="mailto:<?php the_field('email', 'option'); ?>" class="email"><?php the_field('email', 'option'); ?></a>
                </li>
                
                <li>
                    <a href="tel:<?php echo $phone; ?>" class="phone"><?php the_field('phone', 'option'); ?></a>
                </li>
            </ul>
        </div>
        <div id="footer-sidebar4" class="from-s-up-fourcol from-s-up-last threecol last">
            <h3>Social Media</h3>
            <ul class="sm-links">
                <?php while (have_rows('social_media', 'options')): the_row(); ?>
                    <li>
                        <a href="<?php tsf('link') ?>" target="_blank"><?php svg_sub_embed('image', 'sm-svg') ?></a>
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>
    </div>    

    <div class="cf bottom-footer mw-1040 p1">
        <p class="source-org copyright">&copy; <?php echo date('Y'); ?> <a href="<?php bloginfo( 'url' ); ?>"><?php bloginfo( 'name' ); ?></a>.</p>
    </div>

</footer>
            </div> <?php //end #container ?>
                <?php wp_footer(); ?>
        </div> <?php //end .page-wrap ?>
<?php tfo('closing_body_code'); ?>
    </body>
</html> <?php // end page. what a ride! ?>


