<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<?php get_template_part('inc/head'); ?>

<body <?php body_class(); ?>>

<?php tfo('opening_body_code'); ?>

<?php get_template_part('inc/if-ie'); ?>

<div class="page-wrap">
	
	
	<nav role="navigation" class="from-m-up-cf pushy pushy-left top-nav-wrap headroom" id="header">
		<div class="nav-bar from-m-down">
			<a href="#" class="menu-btn bt-menu-trigger"><span>Menu</span></a>
			<a href="<?php bloginfo('url'); ?>" class="home-link">
				<?php svg_options_embed('logo_color'); ?>
			</a>
		</div>
		<div class="cf inner-nav-wrap">
			<a href="<?php bloginfo('url'); ?>" class="home-link from-m-up">
				<?php svg_options_embed('logo_color'); ?>
			</a>
            <?php label_main_nav(); ?>
        </div>
    </nav>
			
    <div class="site-overlay"></div>
		
	<div class="nav-bar from-m-down nav-bar-outside">
		<a href="#" class="menu-btn bt-menu-trigger"><span>Menu</span></a>
		<a href="<?php bloginfo('url'); ?>" class="home-link">
			<?php svg_options_embed('logo_color'); ?>
		</a>
	</div>
		
   	<div id="container" class="">
		